package org.oguz;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by oguz on 29/01/15.
 */
public class GrayscaleImageCreator {

    private int[] data;

    public GrayscaleImageCreator(String imagePath){
        BufferedImage image = new BufferedImage(40,30,
                BufferedImage.TYPE_BYTE_GRAY);


        BufferedImage colorImage = null;
        try {
            colorImage = ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(colorImage != null){

            Graphics g = image.getGraphics();
            g.drawImage(colorImage, 0, 0,40,30,null);
            g.dispose();
/*
            try {
                ImageIO.write(image,"png", new File("/Users/oguz/Downloads/cargraypngmodded2.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
*/
            int[] grayImageArray = new int[image.getWidth()*image.getHeight()];
            for(int y = 0 ; y < image.getHeight(); y++){
                int mul = y* image.getWidth();
                for(int x = 0 ; x < image.getWidth() ; x++ ){
                    grayImageArray[mul + x] = image.getRGB(x,y)&0x000000FF;
                }
            }

            data = grayImageArray;

        }
    }

    int[] getData(){
        return data;
    }
}
