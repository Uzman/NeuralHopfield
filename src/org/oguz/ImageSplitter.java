package org.oguz;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by oguz on 01/02/15.
 */
public class ImageSplitter {

    final int SQUARE_HEGIHT = 40;
    BufferedImage[][] images;

    public ImageSplitter(String filename){
        File f = new File(filename);
        int width,height;
        BufferedImage bufferedImage = null;
        try {
             bufferedImage = ImageIO.read(f);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if(bufferedImage != null){
            width = bufferedImage.getWidth();
            height = bufferedImage.getHeight();
            int y = height/SQUARE_HEGIHT;
            int x = width/SQUARE_HEGIHT;
            images = new BufferedImage[y][x];

            for(int yy = 0 ; yy < y ; yy++){
                for(int xx = 0; xx < x ; xx++){
                    images[y][x] = new BufferedImage(SQUARE_HEGIHT,SQUARE_HEGIHT,bufferedImage.getType());
                    Graphics2D gr = images[y][x].createGraphics();
                    gr.drawImage(bufferedImage,0,0,SQUARE_HEGIHT,SQUARE_HEGIHT,SQUARE_HEGIHT*yy,SQUARE_HEGIHT*xx,SQUARE_HEGIHT*(yy+1),SQUARE_HEGIHT*(xx+1),null);
                }
            }

        }
    }

    class ImagePart{

    }
}
