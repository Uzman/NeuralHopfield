package org.oguz;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.BitSet;

/**
 * Created by oguz on 29/01/15.
 */
public class Main {

    public static void main(String[] Args){


        byte[] q = {12,67};
        BitSet b= BitSet.valueOf(q);
        for(int i = 0 ; i < b.length() ; i++) {
            System.out.print(b.get(i) ? 1 : 0);
        }
        System.out.println();


        /*

        Date start = new Date();
        {
            GrayscaleImageCreator grayscaleImageCreator = new GrayscaleImageCreator("/Users/oguz/Downloads/carpng.png");


            int[] first = grayscaleImageCreator.getData();


            GrayscaleImageCreator grayscaleImageCreator1 = new GrayscaleImageCreator("/Users/oguz/Downloads/carpngmodded.png");

            int[] second = grayscaleImageCreator1.getData();

            GrayscaleImageCreator grayscaleImageCreator2 = new GrayscaleImageCreator("/Users/oguz/Downloads/ornek_bmw.png");

            int[] third = grayscaleImageCreator2.getData();

            GrayscaleImageCreator mercedesGrayscaleImageCreator3 = new GrayscaleImageCreator("/Users/oguz/Downloads/mercedes.png");

            int[] mercedes = mercedesGrayscaleImageCreator3.getData();

            GrayscaleImageCreator mercedesGrayscaleImageCreator4 = new GrayscaleImageCreator("/Users/oguz/Downloads/mercedes_broken.png");

            int[] mercedesBroken = mercedesGrayscaleImageCreator4.getData();

            int diffCount = 0;
            for(int i = 0 ; i < mercedes.length ; i++){
                if(mercedes[i]!= mercedesBroken[i]){
                    diffCount++;
                }
            }

            HopfieldNetwork network = new HopfieldNetwork();
            network.addPattern(first);
            network.addPattern(third);
            network.addPattern(mercedes);



            SimpleMatrix recalledMatrix = network.recall(mercedesBroken);

            if(network.matches(mercedes,recalledMatrix)){
                System.out.println("Match");
            }



            Date finish = new Date();

            System.out.println("Total different pixel count: " + diffCount);
            System.out.println("Total time " + (finish.getTime()-start.getTime()));

        }
        

        */


        {

            ImagePixelColorParser imagePixelColorParser = new ImagePixelColorParser("/Users/oguz/Downloads/aycicegi.png");

            byte[] redAyCicegiPNG = imagePixelColorParser.getRed();
            byte[] greenAyCicegiPNG = imagePixelColorParser.getGreen();
            byte[] blueAycicegiPNG = imagePixelColorParser.getBlue();


            ImagePixelColorParser imagePixelColorParser1 = new ImagePixelColorParser("/Users/oguz/Downloads/aycicegi_broken.png");

            byte[] redModdedAyCicegiPNG = imagePixelColorParser1.getRed();
            byte[] greenModdedAyCicegiPNG = imagePixelColorParser1.getGreen();
            byte[] blueModdedAyCicegiPNG = imagePixelColorParser1.getBlue();

            ImagePixelColorParser imagePixelColorParser2 = new ImagePixelColorParser("/Users/oguz/Downloads/ornek_bmw.png");

            byte[] redBmwPNG = imagePixelColorParser2.getRed();
            byte[] greenBmwPNG = imagePixelColorParser2.getGreen();
            byte[] blueBmwPNG = imagePixelColorParser2.getBlue();

            ImagePixelColorParser imagePixelColorParser3 = new ImagePixelColorParser("/Users/oguz/Downloads/elma.png");

            byte[] redMercedesPNG = imagePixelColorParser3.getRed();
            byte[] greenMercedesPNG = imagePixelColorParser3.getGreen();
            byte[] blueMercedesPNG = imagePixelColorParser3.getBlue();


            ImagePixelColorParser imagePixelColorParser4 = new ImagePixelColorParser("/Users/oguz/Downloads/mercedes_broken.png");

            byte[] redMercedesBrokenPNG = imagePixelColorParser4.getRed();
            byte[] greenMercedesBrokenPNG = imagePixelColorParser4.getGreen();
            byte[] blueMercedesBrokenPNG = imagePixelColorParser4.getBlue();


            AlternativeHopfield redHolder = new AlternativeHopfield(9600);
            AlternativeHopfield blueHolder = new AlternativeHopfield(9600);
            AlternativeHopfield greenHolder = new AlternativeHopfield(9600);




            //redHolder.addPattern(ar);
            //redHolder.addPattern(ar2);
            //redHolder.recall(faulty);

            redHolder.addPattern(redBmwPNG);
            redHolder.addPattern(redAyCicegiPNG);
            redHolder.addPattern(redMercedesPNG);

            greenHolder.addPattern(greenBmwPNG);
            greenHolder.addPattern(greenAyCicegiPNG);
            greenHolder.addPattern(greenMercedesPNG);

            blueHolder.addPattern(blueBmwPNG);
            blueHolder.addPattern(blueAycicegiPNG);
            blueHolder.addPattern(blueMercedesPNG);


            byte[] recalledRed = redHolder.recall(redModdedAyCicegiPNG);
            byte[] recalledGreen = greenHolder.recall(greenModdedAyCicegiPNG);
            byte[] recalledBlue = blueHolder.recall(blueModdedAyCicegiPNG);


            int length = recalledBlue.length-1;
            int[] finalRecoveredImage  = new int[length];
            for(int i = 0 ; i < length ; i++){

                int red = recalledRed[i];
                int blue = recalledBlue[i];
                int green = recalledGreen[i];
                finalRecoveredImage[i] =
                        (0xFF000000)|
                        ((red<<16)&0x00FF0000)|
                                                                ((green<<8)&0x0000FF00)|
                                                        (blue&0x000000FF);


            }

            BufferedImage img = getImageFromArray(finalRecoveredImage,40,30);


            BufferedImage img2 = toBufferedImage(img);

            try {
                ImageIO.write(img,"png", new File("/Users/oguz/Downloads/recovered.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }









    }

    public static BufferedImage getImageFromArray(int[] pixels, int width, int height) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for(int y = 0 ; y < height; y++){
            for(int x = 0 ; x < width ; x++){
                image.setRGB(x,y,pixels[width*y+x]);
            }
        }
        return image;
    }

    public static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }


    private static void startAddingPatterns(AlternativeHopfield network, byte[]... arg){

        for (int i = 0 ; i < arg.length ; i++){
            new Thread(new Runnable() {
                @Override
                public void run() {

                }
            }).start();
        }

    }

}
