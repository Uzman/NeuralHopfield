package org.oguz;

import java.util.BitSet;

/**
 * Created by oguz on 31/01/15.
 */
public class AlternativeHopfield {

    byte[][] weight;


    public AlternativeHopfield(int length){

        weight = new byte[length][length];

    }

    public void addPattern(byte[] pattern) {

        if(pattern.length != weight.length){
            throw new IllegalArgumentException("The argument length must be pattern length + 1(trailing -127)");
        }

        multiplySumZeroDiagonal(pattern);

    }

    private void multiplySumZeroDiagonal(byte[] pattern){

        BitSet set = BitSet.valueOf(pattern);
       //System.out.println();
        int length = set.length()-8;
        for(int y = 0 ; y < length; y++){
            /*System.out.print(set.get(y) ? 1 : -1);
            System.out.print(" ");*/
            for(int x = 0  ; x < length ; x++){
                if(y!=x) {
                    weight[y][x] += (byte) ((set.get(y) ? 1 : -1) * (set.get(x) ? 1 : -1));
                }
            }
        }
    }


    public byte[] recall(byte[] faulty){


        byte[] current = faulty;
        byte[] previous;

        do{
            byte[] array = multiplyWithWeightAndSignum(current);

            previous = current;
            current = array;
        }while (!vectorsAreIdentical(previous,current));

        return current;


    }

    private byte[] multiplyWithWeightAndSignum(byte[] vectorX){
        int length = weight.length;
        BitSet set = BitSet.valueOf(vectorX);


        /*
        //değerinin Vector X ile hiç bir alaksı yok
        VectorX ile aynı boyuttadır ve son elemanı -127'dir.
         */
        BitSet returnSet = BitSet.valueOf(vectorX);

        System.out.println();
        for(int y = 0 ; y <length ; y++){
            int val =  0;
            for(int x = 0 ; x < length ; x++){
                val += weight[x][y]*(set.get(x)?1:-1);
            }
            if(val<0){
                returnSet.set(y,false);
            } else {
                returnSet.set(y,true);
            }
           // System.out.print(returnSet.get(y)? "1":"0");
        }

        byte[] returnVal  = returnSet.toByteArray();
        return returnVal;
    }

    private byte[] signum(byte[] current) {
        for(int i = 0 ; i < current.length ; i++){
            current[i] = (byte) ((current[i]<0)?-1 : 1);
        }
        return current;
    }

    private boolean vectorsAreIdentical(byte[] first, byte[] second){
        if(first.length != second.length)
            return false;
        for(int i  = 0 ; i < first.length ; i++){
            if(first[i]!=second[i])
                return false;
        }
        return true;
    }
}
