package org.oguz;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by oguz on 30/01/15.
 */
public class ImagePixelColorParser {

    private final byte[] green;


    private final byte[] blue;
    private final byte[] red;

    public ImagePixelColorParser(String imagePath){

        byte[] green,blue,red;

        BufferedImage colorImage = null;
        try {
            colorImage = ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        int IMG_WIDTH  =40, IMG_HEIGHT = 30;

        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, colorImage.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(colorImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        colorImage.flush();
        colorImage = null;




        green = new byte[resizedImage.getHeight()*resizedImage.getWidth()+1];
        blue = new byte[resizedImage.getHeight()*resizedImage.getWidth()+1];
        red = new byte[resizedImage.getHeight()*resizedImage.getWidth()+1];



        for(int y = 0 ; y < resizedImage.getHeight(); y++) {
            int mul = y * resizedImage.getWidth();
            for (int x = 0; x < resizedImage.getWidth(); x++) {
                int rgb = resizedImage.getRGB(x, y);
                blue[mul+x] = (byte) (( rgb& 0x000000FF));
                green[mul + x] = (byte) ((rgb & 0x0000FF00)>>8);
                red[mul+ x] = (byte) ((rgb & 0x00FF0000)>>16);
            }
        }

        green[green.length-1] = -127;
        blue[blue.length-1] = -127;
        red[red.length-1] = -127;

        this.green = green;
        this.blue = blue;
        this.red = red;
    }

    public byte[] getRed(){
        return red;
    }

    public byte[] getGreen() {
        return green;
    }

    public byte[] getBlue() {
        return blue;
    }
}
