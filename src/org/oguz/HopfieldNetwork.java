package org.oguz;
/*
import org.ejml.data.DenseMatrix64F;
import org.ejml.simple.SimpleMatrix;
*/
import java.util.BitSet;

/**
 * Created by oguz on 29/01/15.
 */
public class HopfieldNetwork {
/*
    SimpleMatrix weightMatrix;

    long[] weight;

    public HopfieldNetwork(){

    }

    public void addPattern(int[] pattern){
        SimpleMatrix simpleMatrix = new SimpleMatrix(DenseMatrix64F.wrap(pattern.length,1,split(pattern)));
        if(this.weightMatrix == null){
            double[] empty = new double[pattern.length*pattern.length];
            this.weightMatrix = new SimpleMatrix(DenseMatrix64F.wrap(pattern.length,pattern.length,empty));
        }
        simpleMatrix = simpleMatrix.mult(simpleMatrix.transpose());
        this.weightMatrix = this.weightMatrix.plus(simpleMatrix);
        for(int i = 0; i < pattern.length;i++){
            this.weightMatrix.set(i, i, 0);
        }
    }

    public void addPattern(byte[] pattern){

        BitSet set = BitSet.valueOf(pattern);

        SimpleMatrix simpleMatrix = new SimpleMatrix(DenseMatrix64F.wrap(pattern.length,1,split(pattern)));
        if(this.weightMatrix == null){
            double[] empty = new double[pattern.length*pattern.length];
            this.weightMatrix = new SimpleMatrix(DenseMatrix64F.wrap(pattern.length,pattern.length,empty));
        }
        simpleMatrix = simpleMatrix.mult(simpleMatrix.transpose());
        this.weightMatrix = this.weightMatrix.plus(simpleMatrix);
        for(int i = 0; i < pattern.length;i++){
            this.weightMatrix.set(i, i, 0);
        }
    }





    public SimpleMatrix recall(int[] array){
        SimpleMatrix previous;

        SimpleMatrix current = new SimpleMatrix(DenseMatrix64F.wrap(array.length,1,split(array)));
        SimpleMatrix initial = current;

        do{
            SimpleMatrix newMatrix = weightMatrix.mult(current);
            previous = current;
            current = newMatrix;

            signum(current);

        } while(!current.isIdentical(previous,0));

        return current;

    }

    public SimpleMatrix recall(byte[] array){
        SimpleMatrix previous;

        SimpleMatrix current = new SimpleMatrix(DenseMatrix64F.wrap(array.length,1,split(array)));


        do{
            SimpleMatrix newMatrix = weightMatrix.mult(current);
            previous = current;
            current = newMatrix;

            signum(current);

        } while(!current.isIdentical(previous,0));

        return current;

    }

    public byte[] convertToByteArray(SimpleMatrix matrix){
        int rowwCount = matrix.numRows();
        int colCount = matrix.numCols();
        int count = rowwCount* colCount;
        byte[] ret = new byte[count];
        for(int y = 0; y < count; y++ ){
            ret[y] = (byte) matrix.get(y);
        }
        return null;
    }

    private void signum(SimpleMatrix matrix){
        int x = matrix.numRows();
        int y = matrix.numCols();

        for(int yy = 0; yy < y; yy++){
            for(int xx = 0 ; xx < x  ; xx++){
                if(matrix.get(xx,yy)<0){
                    matrix.set(xx,yy,-1);
                } else if(matrix.get(xx,yy)>0){
                    matrix.set(xx,yy,1);
                } else {
                    matrix.set(xx,yy,0);
                }
            }
        }
    }

    double[] split(int[] array){
        double[] returnArray = new double[8*array.length];
        for(int i = 0 ; i <array.length ;  i++){
            for(int index  = 0; index <8 ; index++){
                int ander = (int)Math.pow(2,index);
                if((ander&array[i]) == 0){
                    returnArray[8*i+7-index] = -1;
                } else {
                    returnArray[8*i+7-index] =  1;
                }
            }
        }
        return returnArray;
    }

    double[] split(byte[] array){
        double[] returnArray = new double[8*array.length];
        for(int i = 0 ; i <array.length ;  i++){
            for(int index  = 0; index <8 ; index++){
                int ander = (int)Math.pow(2,index);
                if((ander&array[i]) == 0){
                    returnArray[8*i+7-index] = -1;
                } else {
                    returnArray[8*i+7-index] =  1;
                }
            }
        }
        return returnArray;
    }

    public boolean matches(int[] array, SimpleMatrix matrix){
        SimpleMatrix a = new SimpleMatrix(DenseMatrix64F.wrap(matrix.numRows(),matrix.numCols(),split(array)));
        return matricesAreIdentical(a, matrix);
    }

    public boolean matricesAreIdentical(SimpleMatrix first, SimpleMatrix second){
        int x = first.numRows();
        int y = first.numCols();

        for(int yy = 0; yy < y; yy++){
            for(int xx = 0 ; xx < x  ; xx++) {
                if(first.get(xx,yy)!=second.get(xx,yy))
                    return false;

            }

        }
        return true;
    }
*/
}
